@extends('layouts.master')

@section('content')
    <ul>
        @foreach( $tasks as $task)
        <li>
            <a href ="/tasks/{{ $task->id }}"> <!-- Синтаксис Laravel вместо ?id = -->
                {{ $task->body}}    <!-- Синтаксис Laravel вместо php echo = -->
            </a>
        </li>
        @endforeach
    </ul>
@endsection