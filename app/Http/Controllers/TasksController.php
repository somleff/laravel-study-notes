<?php

namespace App\Http\Controllers;

use App\Task; //Указываем путь на модель используемую в контроллере

class TasksController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }


//route model binding (с привязкой(связыванием)роута к модели)
    public function show(Task $task)    //Task::find(wildcard);
    {
        return view('tasks.show', compact('task'));
    }


//без привязки(связывания)роута к модели
    // public function show($id)
    // {
    //     $task = Task::find($id);
    //     return view('tasks.show', compact('task'));
    // }
}
