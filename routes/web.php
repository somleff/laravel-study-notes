<?php

Route::get('/', 'PostsController@index');
Route::get('/posts/{post}', 'PostsController@show');

// Lesson Laravel 5.4 From Scratch: from 1 to 9
 Route::get('/tasks', 'TasksController@index');
Route::get('/tasks/{task}', 'TasksController@show');


//use App\Task;   //Если не указывать путь к модели, то обращаться к DB нужно с указанием пути

//Варианты роутов без контроллера

// //писать путь можно без /
// Route::get('/tasks', function () {
//     $tasks = Task::all();
//     return view('tasks.index', compact('tasks'));   //можно еще tasks/index
// });

//указан namespace
// Route::get('/tasks/{task}', function ($id) {
//     $task = Task::find($id);
//     return view('tasks.show', compact('task'));
// });


//Запрос в DB напрямую через конструктор запросов
// Route::get('/tasks/{task}', function ($id) {
//     $task = DB::table('tasks')->find($id);
//     return view('tasks.show', compact('task'));
// });

//Запрос к DB с помощью модели Task. Не указан namespace
// Route::get('/tasks/{task}', function ($id) {
//     $task = App\Task::find($id);
//     return view('tasks.show', compact('task'));
// });